package com.albertoserrano.brickwall.model

import com.albertoserrano.domain.constants.Constants.Companion.EMPTY_STRING

data class TeacherView(
        val id: Long,
        val fullName: String,
        val name: String,
        val lastName: String,
        val email: String,
        val school: String,
        val role: String? = EMPTY_STRING
)