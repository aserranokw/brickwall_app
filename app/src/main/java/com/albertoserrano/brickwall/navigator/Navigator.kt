package com.albertoserrano.brickwall.navigator

import android.content.Context
import com.albertoserrano.brickwall.view.activity.EditTeacherActivity
import com.albertoserrano.brickwall.view.activity.LoginActivity
import com.albertoserrano.brickwall.view.activity.MainActivity
import com.albertoserrano.brickwall.view.activity.TeacherDetailActivity

/**
 * Navigator.
 */
fun navigateToTeachersActivity(context: Context) {
    val intent = MainActivity.getCallingIntent(context)
    context.startActivity(intent)
}

fun navigateToLoginActivity(context: Context) {
    val intent = LoginActivity.getCallingIntent(context)
    context.startActivity(intent)
}

fun navigateToTeacherDetailActivity(context: Context, id: Long) {
    val intent = TeacherDetailActivity.getCallingIntent(context, id)
    context.startActivity(intent)
}

fun navigateToEditTeacherActivity(context: Context, id: Long) {
    val intent = EditTeacherActivity.getCallingIntent(context, id)
    context.startActivity(intent)
}