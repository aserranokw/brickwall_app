package com.albertoserrano.brickwall.view.activity

import android.content.Context
import android.content.Intent
import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.extension.showAlertDialog
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.brickwall.presenter.EditTeacherPresenter
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.android.synthetic.main.activity_edit_teacher.*

class EditTeacherActivity : RootActivity<EditTeacherPresenter.View>(), EditTeacherPresenter.View {
    // TODO - toModify (just Enter From SchoolDetail to its teachers)
    companion object {
        private const val TEACHER_ID = "TEACHER_ID"

        fun getCallingIntent(context: Context, id: Long): Intent {
            val intent = Intent(context, EditTeacherActivity::class.java)
            intent.putExtra(TEACHER_ID, id)
            return intent
        }
    }

    override val progress: View by lazy { progressView }
    override val presenter: EditTeacherPresenter by instance()
    override val layoutResourceId: Int = R.layout.activity_edit_teacher
    override val activityModule: Kodein.Module = Kodein.Module {
        bind<EditTeacherPresenter>() with provider() {
            EditTeacherPresenter(
                    id = getTeacherId(),
                    modifyTeacherUseCase = instance(),
                    retrieveTeacherUseCase = instance(),
                    view = this@EditTeacherActivity,
                    errorHandler = instance()
            )
        }
    }

    override fun initializeUI() {
        // Nothing to do
    }

    override fun registerListeners() {
        discardChanges.setOnClickListener {
            presenter.discardChangesClicked()
        }
        saveChanges.setOnClickListener {
            presenter.saveChangesClicked()
        }
        deleteButton.setOnClickListener {
            presenter.deleteTeacherClicked()
        }
    }

    override fun getTeacherId(): Long =
            intent.extras.getLong(EditTeacherActivity.TEACHER_ID)

    override fun showTeacher(teacher: TeacherView) {
        teachersName.setText(teacher.name)
        teachersLastName.setText(teacher.lastName)
        teachersEmail.setText(teacher.email)
        teachersRole.text = teacher.role
        teachersSchool.text = teacher.school
    }

    override fun onBackPressed() {
        presenter.discardChangesClicked()
    }

    override fun showSaveTeacherDialog() {
        showAlertDialog(
                title = getString(R.string.save_change_title),
                message = getString(R.string.save_changes_msg),
                okButton = getString(R.string.yes),
                negativeButton = getString(R.string.no),
                positive = { presenter.saveTeacherInSystem() },
                negative = { }
        )
    }

    override fun showDiscardTeacherDialog() {
        showAlertDialog(
                title = getString(R.string.discard_change_title),
                message = getString(R.string.discard_changes_msg),
                okButton = getString(R.string.yes),
                negativeButton = getString(R.string.no),
                positive = { presenter.goToTheBackScreen() },
                negative = { }
        )
    }

    override fun showDeleteTeacherDialog() {
        showAlertDialog(
                title = getString(R.string.delete_change_title),
                message = getString(R.string.delete_changes_msg),
                okButton = getString(R.string.yes),
                negativeButton = getString(R.string.no),
                positive = { presenter.goToTheBackScreen() },
                negative = { }
        )
    }


    override fun comingBack() {
        finish()
    }

    override fun getInfoTeacher() {
        val name: String = teachersName.text.toString()
        val lastName: String = teachersLastName.text.toString()
        val email: String = teachersEmail.text.toString()
        presenter.saveViewTeacher(name, lastName, email)
    }

    override fun hideDeleteButton() {
        deleteButton.visibility = View.INVISIBLE
    }

    override fun showDeleteButton() {
        deleteButton.visibility = View.VISIBLE
    }
}