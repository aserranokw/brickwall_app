package com.albertoserrano.brickwall.view.fragment

import android.support.v7.widget.LinearLayoutManager
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.extension.hideMe
import com.albertoserrano.brickwall.extension.showMe
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.brickwall.navigator.navigateToTeacherDetailActivity
import com.albertoserrano.brickwall.presenter.TeachersListPresenter
import com.albertoserrano.brickwall.view.adapter.ItemTeacherAdapter
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.android.synthetic.main.fragment_teachers.*

//enum class Item {
//    TASK, COMPETITION, SCHOOL, TEACHER
//}

class TeachersFragment : RootFragment<TeachersListPresenter.View>(), TeachersListPresenter.View {

    override val presenter: TeachersListPresenter by instance()

    override val layoutResourceId: Int = R.layout.fragment_teachers

    override val fragmentModule: Kodein.Module = Kodein.Module {
        bind<TeachersListPresenter>() with provider {
            TeachersListPresenter(
                    retrieveTeachersUseCase = instance(),
                    view = this@TeachersFragment,
                    errorHandler = instance()
            )
        }
    }

    private val adapter = ItemTeacherAdapter {
        presenter.onTeacherClicked(it)
    }

    override fun initializeUI() {
        teachers.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        teachers.adapter = adapter
    }

    override fun registerListeners() {
        // Nothing to do yet
    }

    override fun showTeachers(teachers: List<TeacherView>) {
        adapter.replace(teachers.toMutableList())
    }


    override fun showProgress() = progressView.showMe()

    override fun hideProgress() = progressView.hideMe()

    override fun goToTeacherDetailScreen(id: Long) {
        activity?.let {
            navigateToTeacherDetailActivity(it, id)
        }
    }
}
