package com.albertoserrano.brickwall.view.adapter

import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.model.TeacherView
import kotlinx.android.synthetic.main.item_teacher.view.*

class ItemTeacherAdapter(onItemClickListener: (TeacherView) -> Unit = {}) : RootAdapter<TeacherView>(onItemClickListener = onItemClickListener) {

    override val itemLayoutId: Int = R.layout.item_teacher

    override fun viewHolder(view: View): RootViewHolder<TeacherView> = TeacherViewHolder(view)

    class TeacherViewHolder(view: View) : RootViewHolder<TeacherView>(view) {
        override fun bind(model: TeacherView) {
            itemView.name.text = model.fullName
            itemView.email.text = model.email
        }
    }
}

