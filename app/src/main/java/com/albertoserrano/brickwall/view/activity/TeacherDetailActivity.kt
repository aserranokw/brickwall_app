package com.albertoserrano.brickwall.view.activity

import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.extension.showAlertDialog
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.brickwall.navigator.navigateToEditTeacherActivity
import com.albertoserrano.brickwall.navigator.navigateToLoginActivity
import com.albertoserrano.brickwall.presenter.TeacherDetailPresenter
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.android.synthetic.main.activity_teacherdetail.*

class TeacherDetailActivity : RootActivity<TeacherDetailPresenter.View>(), TeacherDetailPresenter.View {
    // TODO - toModify (just Enter From SchoolDetail to its teachers)
    companion object {
        private const val TEACHER_ID = "TEACHER_ID"

        fun getCallingIntent(context: Context, id: Long): Intent {
            val intent = Intent(context, TeacherDetailActivity::class.java)
            intent.putExtra(TEACHER_ID, id)
            return intent
        }
    }

    override val progress: View by lazy { progressView }
    override val presenter: TeacherDetailPresenter by instance()
    override val layoutResourceId: Int = R.layout.activity_teacherdetail

    override val activityModule: Kodein.Module = Kodein.Module {
        bind<TeacherDetailPresenter>() with provider {
            TeacherDetailPresenter(
                    id = getTeacherId(),
                    retrieveTeacherUseCase = instance(),
                    modifyTeacherUseCase = instance(),
                    view = this@TeacherDetailActivity,
                    errorHandler = instance()
            )
        }
    }

    override fun initializeUI() {
        // Nothing to do yet
    }

    override fun registerListeners() {
        editButton.setOnClickListener {
            presenter.onEditButtonClicked(getTeacherId())
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.teacher_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.item_close_session) {
            presenter.onLogOutClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showCloseSessionDialog() =
            showAlertDialog(
                    title = getString(R.string.log_out),
                    message = getString(R.string.log_out_msg),
                    okButton = getString(R.string.yes),
                    negativeButton = getString(R.string.no),
                    positive = { navigateToLoginActivity(context = this) }
            )

    override fun getTeacherId(): Long =
            intent.extras.getLong(TEACHER_ID)

    override fun showTeacher(teacher: TeacherView) {
        teachersFullName.text = teacher.fullName
        teachersEmail.text = teacher.email
        teachersRole.text = teacher.role
        teachersSchool.text = teacher.school
        teachersSchool.setOnClickListener {
            // TODO presenter.goToDetailSchoolScreen(context = this)
        }
    }

    override fun goToEditScreen(id: Long) {
        navigateToEditTeacherActivity(this, id)
    }

    override fun hideEditButton() {
        editButton.visibility = View.INVISIBLE
    }

    override fun showEditButton() {
        editButton.visibility = View.VISIBLE
    }
}