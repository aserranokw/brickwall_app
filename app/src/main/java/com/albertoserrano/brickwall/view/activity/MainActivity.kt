package com.albertoserrano.brickwall.view.activity

import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.extension.showAlertDialog
import com.albertoserrano.brickwall.navigator.navigateToLoginActivity
import com.albertoserrano.brickwall.navigator.navigateToTeacherDetailActivity
import com.albertoserrano.brickwall.presenter.MainPresenter
import com.albertoserrano.brickwall.view.adapter.ViewPagerAdapter
import com.albertoserrano.brickwall.view.fragment.TeachersFragment
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : RootActivity<MainPresenter.View>(), MainPresenter.View {

    companion object {
        fun getCallingIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    override val progress: View
        get() = TODO("Not implemented")

    override val presenter: MainPresenter by instance()

    override val layoutResourceId: Int = R.layout.activity_main

    override val activityModule: Kodein.Module = Kodein.Module {
        bind<MainPresenter>() with provider {
            MainPresenter(
                    settings = instance(),
                    view = this@MainActivity,
                    errorHandler = instance()
            )
        }
    }

    override fun initializeUI() {
        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(
                title = "teachers",
                fragment = TeachersFragment())

        viewPager.adapter = viewPagerAdapter
        tab.setupWithViewPager(viewPager)
        tab.getTabAt(0)?.text = ""
        tab.getTabAt(0)?.setIcon(R.drawable.ic_teacher)
    }

    override fun registerListeners() {
        profile.setOnClickListener {
            presenter.onProfileClicked()
        }
    }


    override fun showCloseSessionDialog() {
        showAlertDialog(
                title = getString(R.string.log_out),
                message = getString(R.string.log_out_msg),
                okButton = getString(R.string.yes),
                negativeButton = getString(R.string.no),
                positive = { navigateToLoginActivity(context = this) }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.teacher_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.item_close_session) {
            presenter.onLogOutClicked()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goToProfileScreen(currentTeacher: Long) {
        navigateToTeacherDetailActivity(context = this, id = currentTeacher)
    }
}