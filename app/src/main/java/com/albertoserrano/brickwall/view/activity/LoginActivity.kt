package com.albertoserrano.brickwall.view.activity

import android.content.Context
import android.content.Intent
import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.navigator.navigateToTeachersActivity
import com.albertoserrano.brickwall.presenter.LoginPresenter
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : RootActivity<LoginPresenter.View>(), LoginPresenter.View {
    companion object {
        fun getCallingIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    override val progress: View
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override val presenter: LoginPresenter by instance()
    override val layoutResourceId: Int = R.layout.activity_login
    override val activityModule: Kodein.Module = Kodein.Module {
        bind<LoginPresenter>() with provider {
            LoginPresenter(
                    settings = instance(),
                    loginUseCase = instance(),
                    view = this@LoginActivity,
                    errorHandler = instance()
            )
        }
    }

    override fun initializeUI() {
        // Nothing to do yet
    }

    override fun registerListeners() {
        btn_login_admin.setOnClickListener { presenter.onAdminClicked() }
        btn_login_manager1.setOnClickListener { presenter.onManagerClicked() }
        btn_login_manager2.setOnClickListener { presenter.onManager2Clicked() }
        btn_login_educator1.setOnClickListener { presenter.onEducatorClicked() }
    }

    override fun goToTeachersScreen() {
        navigateToTeachersActivity(context = this)
        finish()
    }
}


