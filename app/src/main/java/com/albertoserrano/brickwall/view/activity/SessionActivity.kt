package com.albertoserrano.brickwall.view.activity

import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.extension.showAlertDialog
import com.albertoserrano.brickwall.navigator.navigateToLoginActivity
import com.albertoserrano.brickwall.presenter.SessionPresenter


abstract class SessionActivity : RootActivity<SessionPresenter.View>(), SessionPresenter.View {
    companion object {
        protected const val TEACHER_ID = "TEACHER_ID"
    }

    override fun getTeacherId(): Long =
            intent.extras.getLong(SessionActivity.TEACHER_ID)

    override fun showCloseSessionDialog() {
        showAlertDialog(
                title = getString(R.string.log_out),
                message = getString(R.string.log_out_msg),
                okButton = getString(R.string.yes),
                negativeButton = getString(R.string.no),
                positive = { navigateToLoginActivity(context = this) }
        )
    }
}