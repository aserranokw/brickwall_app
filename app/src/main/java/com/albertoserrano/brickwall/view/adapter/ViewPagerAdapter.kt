package com.albertoserrano.brickwall.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {


    private val titles: MutableList<String> = mutableListOf()
    private val fragments: MutableList<Fragment> = mutableListOf()

    /**
     * Return the Fragment associated with a specified position.
     */
    override fun getItem(position: Int): Fragment = fragments[position]

    /**
     * Return the number of views available.
     */
    override fun getCount(): Int = fragments.size

    fun addFragment(title: String, fragment: Fragment) {
        fragments.add(fragment)
        titles.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position]
    }
}