package com.albertoserrano.brickwall.view.activity

import android.os.Handler
import android.view.View
import com.albertoserrano.brickwall.R
import com.albertoserrano.brickwall.navigator.navigateToLoginActivity
import com.albertoserrano.brickwall.navigator.navigateToTeachersActivity
import com.albertoserrano.brickwall.presenter.SplashPresenter
import com.albertoserrano.brickwall.presenter.SplashPresenter.Companion.DURATION_SPLASH
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider

class SplashActivity : RootActivity<SplashPresenter.View>(), SplashPresenter.View {
    override val progress: View
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    private var mDelayHandler: Handler? = null
    override val presenter: SplashPresenter by instance()
    override val layoutResourceId: Int = R.layout.activity_splash
    override val activityModule: Kodein.Module = Kodein.Module {
        bind<SplashPresenter>() with provider {
            SplashPresenter(
                    settings = instance(),
                    view = this@SplashActivity,
                    errorHandler = instance()
            )
        }
    }

    override fun initializeUI() {
        mDelayHandler = Handler()

        mDelayHandler?.postDelayed({
            presenter.onDelayFinish()
        }, DURATION_SPLASH)
    }

    override fun registerListeners() {
        // Nothing to do yet
    }


    override fun goToTeachersScreen() {
        navigateToTeachersActivity(context = this)
    }

    override fun goToLoginScreen() {
        navigateToLoginActivity(context = this)
    }


}