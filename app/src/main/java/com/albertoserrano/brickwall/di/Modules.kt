package com.albertoserrano.brickwall.di

import android.content.Context
import com.albertoserrano.brickwall.BuildConfig
import com.albertoserrano.brickwall.error.AndroidErrorHandler
import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.brickwall.executor.RxExecutor
import com.albertoserrano.data.datasources.local.DatabaseRepository
import com.albertoserrano.data.datasources.local.RealmDataSource
import com.albertoserrano.data.datasources.remote.*
import com.albertoserrano.data.datasources.settings.Settings
import com.albertoserrano.data.datasources.settings.SharedPreferences
import com.albertoserrano.domain.constants.BuildType
import com.albertoserrano.domain.constants.Constants.Companion.preferencesName
import com.albertoserrano.domain.constants.buildType
import com.albertoserrano.domain.executor.Executor
import com.albertoserrano.domain.interactor.usecases.LoginUseCase
import com.albertoserrano.domain.interactor.usecases.ModifyTeacherUseCase
import com.albertoserrano.domain.interactor.usecases.RetrieveTeacherUseCase
import com.albertoserrano.domain.interactor.usecases.RetrieveTeachersUseCase
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton

/**
 * Modules
 */
fun appModule(context: Context) = Kodein.Module {
    bind<Context>() with singleton { context }
    bind<Executor>() with singleton { RxExecutor() }
    bind<ErrorHandler>() with singleton { AndroidErrorHandler(context = context) }
    bind<BuildType>() with singleton { buildType(BuildConfig.BUILD_TYPE) }
}

val domainModule = Kodein.Module {
    bind() from singleton { RetrieveTeachersUseCase(repository = instance(), executor = instance()) }
    bind() from singleton { RetrieveTeacherUseCase(repository = instance(), executor = instance()) }
    bind() from singleton { LoginUseCase(repository = instance(), executor = instance()) }
    bind() from singleton { ModifyTeacherUseCase(repository = instance(), executor = instance()) }
}

val dataModule = Kodein.Module {
    bind<com.albertoserrano.domain.repository.TeacherRepository>() with singleton {
        com.albertoserrano.data.repositories.TeachersRepository(
                remote = instance(),
                local = instance(),
                settings = instance())
    }

    bind<Settings>() with singleton {
        SharedPreferences(context = instance(), name = preferencesName(buildType = instance()))
    }

    bind<DatabaseRepository>() with singleton { RealmDataSource() }

    bind<NetworkRepository>() with singleton {
        when (instance<BuildType>()) {
            BuildType.MOCK -> MockNetworkDataSource()
            BuildType.DEBUG -> BrickWallNetworkDataSource(bwService = instance())
            BuildType.RELEASE -> TODO()
        }
    }
    bind<BWService>() with singleton { createService<BWService>(endPoint = BWService.END_POINT) }

}
