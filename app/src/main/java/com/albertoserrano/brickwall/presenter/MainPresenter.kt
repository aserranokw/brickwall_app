package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.data.datasources.settings.Settings

class MainPresenter(private val settings: Settings,
                    view: MainPresenter.View,
                    errorHandler: ErrorHandler) :
        RootPresenter<MainPresenter.View>(view = view, errorHandler = errorHandler) {

    override fun initialize() {
        // Nothing to do yet
    }

    override fun resume() {
        // Nothing to do yet
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        // Nothing to do yet
    }

    fun onLogOutClicked() {
        view.showCloseSessionDialog()
    }

    fun onProfileClicked() {
        view.goToProfileScreen(settings.getTeacher().id)
    }


    interface View : RootPresenter.View {
        fun showCloseSessionDialog()
        fun goToProfileScreen(currentTeacher: Long)
    }
}