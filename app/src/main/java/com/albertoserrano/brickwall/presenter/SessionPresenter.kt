package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler

abstract class SessionPresenter<out V : SessionPresenter.View>(errorHandler: ErrorHandler, view: V) :
        RootPresenter<SessionPresenter.View>(view = view, errorHandler = errorHandler) {


    interface View : RootPresenter.View {
        fun getTeacherId(): Long
        fun showCloseSessionDialog()
    }

}
