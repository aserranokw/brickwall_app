package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.data.datasources.settings.Settings
import com.albertoserrano.domain.interactor.usecases.LoginUseCase

class LoginPresenter(private val loginUseCase: LoginUseCase,
                     private val settings: Settings,
                     view: LoginPresenter.View,
                     errorHandler: ErrorHandler) :
        RootPresenter<LoginPresenter.View>(view = view, errorHandler = errorHandler) {

    override fun initialize() {
        settings.clear()
    }

    override fun resume() {
        // Nothing to do yet
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        loginUseCase.clear()
    }

    fun onAdminClicked() = saveTeacher(1)
    fun onManagerClicked() = saveTeacher(2)
    fun onManager2Clicked() = saveTeacher(3)
    fun onEducatorClicked() = saveTeacher(4)

    private fun saveTeacher(id: Int) {
        loginUseCase.execute(id,
                onComplete = { view.goToTeachersScreen() },
                onError = onError { view.showError(it) }
        )
    }

    interface View : RootPresenter.View {
        fun goToTeachersScreen()
    }
}