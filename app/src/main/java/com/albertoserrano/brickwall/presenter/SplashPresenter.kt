package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.data.datasources.settings.Settings

class SplashPresenter(private val settings: Settings,
                      view: SplashPresenter.View,
                      errorHandler: ErrorHandler) :
        RootPresenter<SplashPresenter.View>(view = view, errorHandler = errorHandler) {

    companion object {
        const val DURATION_SPLASH: Long = 800
    }

    override fun initialize() {
        // Nothing to do yet
    }

    override fun resume() {
        // Nothing to do yet
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        // Nothing to do yet
    }

    fun onDelayFinish() {
        if (settings.hasTeacher()) {
            view.goToTeachersScreen()
        } else {
            view.goToLoginScreen()
        }
    }

    interface View : RootPresenter.View {
        fun goToTeachersScreen()
        fun goToLoginScreen()
    }

}

