package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.brickwall.mappers.toTeacherView
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.domain.interactor.usecases.RetrieveTeachersUseCase

class TeachersListPresenter(private val retrieveTeachersUseCase: RetrieveTeachersUseCase,
                            view: TeachersListPresenter.View,
                            errorHandler: ErrorHandler) :
        RootPresenter<TeachersListPresenter.View>(view = view, errorHandler = errorHandler) {

    override fun initialize() {
        update()
    }

    private fun update() {
        view.showProgress()
        retrieveTeachersUseCase.execute(
                onSuccess = { teachers ->
                    view.showTeachers(teachers.map { teacher -> teacher.toTeacherView() })
                    view.hideProgress()
                },
                onError = onError { view.showError(it) }
        )
    }

    override fun resume() {
        update()
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        retrieveTeachersUseCase.clear()
    }

    fun onTeacherClicked(teacher: TeacherView) {
        view.goToTeacherDetailScreen(teacher.id)
    }


    interface View : RootPresenter.View {
        fun showTeachers(teachers: List<TeacherView>)
        fun goToTeacherDetailScreen(id: Long)
    }
}