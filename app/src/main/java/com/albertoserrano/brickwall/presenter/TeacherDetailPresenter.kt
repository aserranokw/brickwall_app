package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.brickwall.mappers.toTeacherView
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.domain.interactor.usecases.ModifyTeacherUseCase
import com.albertoserrano.domain.interactor.usecases.RetrieveTeacherUseCase
import com.albertoserrano.domain.models.Teacher

class TeacherDetailPresenter(private val id: Long,
                             private val retrieveTeacherUseCase: RetrieveTeacherUseCase,
                             private val modifyTeacherUseCase: ModifyTeacherUseCase,
                             view: TeacherDetailPresenter.View,
                             errorHandler: ErrorHandler) : RootPresenter<TeacherDetailPresenter.View>(view = view, errorHandler = errorHandler) {

    lateinit var teacher: Teacher

    override fun initialize() {
        update()

    }

    private fun update() {
        view.showProgress()
        retrieveTeacherUseCase.execute(
                id = id,
                onSuccess = {
                    teacher = it
                    view.showTeacher(it.toTeacherView())
                    view.hideProgress()
                    initializeEditButton()
                },
                onError = onError { view.showError(it) }
        )
    }

    private fun initializeEditButton() {
        if (modifyTeacherUseCase.isModifiable(teacher))
            view.showEditButton()
        else
            view.hideEditButton()
    }

    override fun resume() {
        update()
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        retrieveTeacherUseCase.clear()
        modifyTeacherUseCase.clear()
    }

    fun onLogOutClicked() {
        view.showCloseSessionDialog()
    }

    fun onEditButtonClicked(id: Long) {
        view.goToEditScreen(id)
    }

    interface View : RootPresenter.View {
        fun showCloseSessionDialog()
        fun goToEditScreen(id: Long)
        fun getTeacherId(): Long
        fun showTeacher(teacher: TeacherView)
        fun hideEditButton()
        fun showEditButton()
    }

}
