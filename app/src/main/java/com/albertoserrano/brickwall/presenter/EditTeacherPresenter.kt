package com.albertoserrano.brickwall.presenter

import com.albertoserrano.brickwall.error.ErrorHandler
import com.albertoserrano.brickwall.mappers.toTeacherView
import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.domain.interactor.usecases.ModifyTeacherUseCase
import com.albertoserrano.domain.interactor.usecases.RetrieveTeacherUseCase
import com.albertoserrano.domain.models.Teacher

class EditTeacherPresenter(private val id: Long,
                           private val retrieveTeacherUseCase: RetrieveTeacherUseCase,
                           private val modifyTeacherUseCase: ModifyTeacherUseCase,
                           view: EditTeacherPresenter.View,
                           errorHandler: ErrorHandler) : RootPresenter<EditTeacherPresenter.View>(view = view, errorHandler = errorHandler) {

    lateinit var teacher: Teacher

    override fun initialize() {
        view.showProgress()
        retrieveTeacherUseCase.execute(
                id = id,
                onSuccess = { it ->
                    teacher = it
                    view.showTeacher(teacher.toTeacherView())
                    view.hideProgress()
                    initializeDeleteButton()
                },
                onError = onError { view.showError(it) }
        )
    }

    private fun initializeDeleteButton() {
        if (modifyTeacherUseCase.isRemovable(teacher))
            view.showDeleteButton()
        else
            view.hideDeleteButton()
    }

    override fun resume() {
        // Nothing to do yet
    }

    override fun stop() {
        // Nothing to do yet
    }

    override fun destroy() {
        retrieveTeacherUseCase.clear()
        modifyTeacherUseCase.clear()
    }

    fun saveChangesClicked() {
        view.showSaveTeacherDialog()
    }

    fun discardChangesClicked() {
        view.showDiscardTeacherDialog()
    }

    fun deleteTeacherClicked() {
        view.showDeleteTeacherDialog()
    }

    fun goToTheBackScreen() {
        view.comingBack()
    }

    fun saveViewTeacher(name: String, lastName: String, email: String) {
        teacher.name = name
        teacher.lastName = lastName
        teacher.email = email
    }

    fun saveTeacherInSystem() {
        view.getInfoTeacher()
        view.showProgress()
        modifyTeacherUseCase.execute(
                teacher = teacher,
                onComplete =
                {
                    view.hideProgress()
                    view.comingBack()
                },
                onError = onError
                { view.showError(it) })

    }

    interface View : RootPresenter.View {
        fun getTeacherId(): Long
        fun showTeacher(teacher: TeacherView)
        fun comingBack()
        fun showSaveTeacherDialog()
        fun showDiscardTeacherDialog()
        fun showDeleteTeacherDialog()
        fun hideDeleteButton()
        fun showDeleteButton()
        fun getInfoTeacher()
    }
}
