package com.albertoserrano.brickwall.extension

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.albertoserrano.domain.constants.Constants.Companion.EMPTY_STRING

/**
 * ContextExtensions
 */

/**
 * Context
 * */
fun Context.toast(text: String, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, text, length).show()
}

fun Context.toast(textId: Int, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, textId, length).show()
}

fun Context.toPx(dp: Int): Int = resources.getDimensionPixelSize(dp)


/**
 * Fragments
 * */
fun Fragment.toast(text: String, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(activity, text, length).show()
}

fun Fragment.toast(textId: Int, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(activity, textId, length).show()
}

/**
 * AlertDialog
 */
fun Context.showAlertDialog(title: String,
                            message: String,
                            okButton: String,
                            positive: () -> Unit = {},
                            negativeButton: String = EMPTY_STRING,
                            negative: () -> Unit = {},
                            cancelable: Boolean = true) {
    AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(okButton) { dialog, _ ->
                positive()
                dialog.dismiss()
            }
            .setNegativeButton(negativeButton) { dialog, _ ->
                negative()
                dialog.dismiss()
            }.setCancelable(cancelable)
            .show()
}