package com.albertoserrano.brickwall.error

import android.content.Context
import com.albertoserrano.brickwall.R
import com.albertoserrano.domain.exceptions.PermissionDenied

/**
 * AndroidErrorHandler.
 */
class AndroidErrorHandler(val context: Context) : ErrorHandler {
    override fun convert(e: Exception): String =
            when (e) {
                is PermissionDenied -> context.getString(R.string.denied_permissions)
                else -> context.getString(R.string.default_error)
            }

}
