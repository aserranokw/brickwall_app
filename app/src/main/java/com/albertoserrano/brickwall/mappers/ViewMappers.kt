package com.albertoserrano.brickwall.mappers

import com.albertoserrano.brickwall.model.TeacherView
import com.albertoserrano.domain.constants.Constants.Companion.EMPTY_STRING
import com.albertoserrano.domain.models.Role
import com.albertoserrano.domain.models.Teacher

fun Teacher.toTeacherView(): TeacherView = TeacherView(
        fullName = "$name $lastName",
        name = name ?: EMPTY_STRING,
        lastName = lastName ?: EMPTY_STRING,
        email = email ?: EMPTY_STRING,
        school = school?.toString() ?: EMPTY_STRING,
        id = id,
        role = when (role) {
            Role.ROLE_EDUCATOR -> "Docente";
            Role.ROLE_MANAGER -> "Responsable de centro";
            Role.ROLE_ADMIN -> "Administrador del sistema";
            else -> EMPTY_STRING
        }
)