package com.albertoserrano.data.datasources.remote

import com.albertoserrano.data.models.TeachersResponseDto
import io.reactivex.Single
import retrofit2.http.GET

interface BWService {

    companion object {
        const val END_POINT = "http://localhost:27017/"
    }

    @GET("teachers")
    fun retrieveTeachers(): Single<TeachersResponseDto>

}