package com.albertoserrano.data.datasources.settings

import com.albertoserrano.domain.models.Teacher

interface Settings {

    fun setTeacher(teacher: Teacher)
    fun getTeacher(): Teacher
    fun hasTeacher(): Boolean
    fun removeTeacher()
    fun clear()

}