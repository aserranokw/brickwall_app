package com.albertoserrano.data.datasources.local

import com.albertoserrano.domain.models.Teacher
import io.reactivex.Completable
import io.reactivex.Maybe

interface DatabaseRepository {

    fun saveTeachers(teachers: List<Teacher>): Completable
    fun saveTeacher(teacher: Teacher): Completable
    fun getTeachers(): Maybe<List<Teacher>>
    fun getTeacher(id: Long): Maybe<Teacher>

}