package com.albertoserrano.data.datasources.remote

import com.albertoserrano.domain.models.Teacher
import io.reactivex.Single

interface NetworkRepository {
    fun retrieveTeachers(): Single<List<Teacher>>

    fun retrieveScore(id: String): Single<Int>

    fun doLogin(id: Int): Single<Teacher>
}