package com.albertoserrano.data.datasources.remote

import com.albertoserrano.data.mappers.dto.toTeacher
import com.albertoserrano.domain.models.Teacher
import io.reactivex.Single

class BrickWallNetworkDataSource(private val bwService: BWService) : NetworkRepository {
    override fun doLogin(id: Int): Single<Teacher> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveScore(id: String): Single<Int> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveTeachers(): Single<List<Teacher>> =
            bwService.retrieveTeachers()
                    .map { teachersResponseDto -> teachersResponseDto.teachers.map { teacherDto -> teacherDto.toTeacher() } }

}