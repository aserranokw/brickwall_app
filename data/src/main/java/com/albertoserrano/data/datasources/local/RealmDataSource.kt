package com.albertoserrano.data.datasources.local

import com.albertoserrano.data.mappers.dao.toTeacher
import com.albertoserrano.data.mappers.dao.toTeacherDao
import com.albertoserrano.data.models.TeacherDao
import com.albertoserrano.data.save
import com.albertoserrano.domain.models.Teacher
import io.reactivex.Completable
import io.reactivex.Maybe

class RealmDataSource : DatabaseRepository {

    override fun getTeachers(): Maybe<List<Teacher>> = RxRealm.getList {
        it.where(TeacherDao::class.java)
                .findAll()
    }.map { list -> list.map { item -> item.toTeacher() } }


    override fun saveTeachers(teachers: List<Teacher>): Completable {
        teachers.map {
            val teacherDao = it.toTeacherDao()
            //    teacherDao.id = teacherDao.nextId()
            teacherDao.save()
            teacherDao
        }
        return Completable.complete()
    }

    override fun saveTeacher(teacher: Teacher): Completable {
        teacher.toTeacherDao().save()
        return Completable.complete()
    }

    override fun getTeacher(id: Long): Maybe<Teacher> = RxRealm.getElement { realm ->
        realm.where(TeacherDao::class.java).equalTo("id", id).findFirst()
    }.map { teacherDao -> teacherDao.toTeacher() }
}