package com.albertoserrano.data.datasources.remote

import com.albertoserrano.domain.constants.Constants
import com.albertoserrano.domain.models.Role
import com.albertoserrano.domain.models.Teacher
import io.reactivex.Single

class MockNetworkDataSource : NetworkRepository {
    override fun retrieveScore(id: String): Single<Int> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveTeachers(): Single<List<Teacher>> =
            Single.just(
                    mutableListOf(
                            Teacher(
                                    id = Constants.DEFAULT_LONG,
                                    name = "Alberto",
                                    lastName = "Serrano",
                                    email = "aserranokw@alumnos.unex.es",
                                    school = null,
                                    role = Role.ROLE_ADMIN),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 1,
                                    name = "Mateo",
                                    lastName = "Borreguero",
                                    email = "mateo@eugeniofrutos.es",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 2,
                                    name = "Eusebio",
                                    lastName = "López",
                                    email = "eusebio@sangregorio.es",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 3,
                                    name = "Federico",
                                    lastName = "Puerto",
                                    email = "federico",
                                    school = null,
                                    role = Role.ROLE_EDUCATOR),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 4,
                                    name = "Jesús",
                                    lastName = "Pérez",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 5,
                                    name = "Vicente",
                                    lastName = "Pino",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 6,
                                    name = "Cristina",
                                    lastName = "Gonzalo",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 7,
                                    name = "Victoria",
                                    lastName = "Muñoz",
                                    school = null,
                                    role = Role.ROLE_MANAGER),
                            Teacher(
                                    id = Constants.DEFAULT_LONG + 8,
                                    name = "Daniel",
                                    lastName = "Gómez",
                                    school = null,
                                    role = Role.ROLE_EDUCATOR)
                    ))

    override fun doLogin(id: Int): Single<Teacher> =
            Single.just(when (id) {
                1 -> Teacher(
                        id = Constants.DEFAULT_LONG,
                        name = "Alberto",
                        lastName = "Serrano",
                        email = "aserranokw@alumnos.unex.es",
                        school = null,
                        role = Role.ROLE_ADMIN)
                2 -> Teacher(
                        id = Constants.DEFAULT_LONG + 1,
                        name = "Mateo",
                        lastName = "Borreguero",
                        email = "mateo@eugeniofrutos.es",
                        school = null,
                        role = Role.ROLE_MANAGER)
                3 -> Teacher(
                        id = Constants.DEFAULT_LONG + 2,
                        name = "Eusebio",
                        lastName = "López",
                        email = "eusebio@sangregorio.es",
                        school = null,
                        role = Role.ROLE_MANAGER)
                4 -> Teacher(
                        id = Constants.DEFAULT_LONG + 8,
                        name = "Daniel",
                        lastName = "Gómez",
                        school = null,
                        role = Role.ROLE_EDUCATOR)
                else -> throw Exception()
            })
}