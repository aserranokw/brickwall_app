package com.albertoserrano.data.datasources.settings

import android.content.Context
import com.albertoserrano.domain.constants.Constants.Companion.EMPTY_STRING
import com.albertoserrano.domain.models.Teacher
import com.google.gson.Gson

class SharedPreferences(context: Context, name: String) : Settings {

    companion object {
        const val TEACHER_KEY = "TEACHER_KEY"
    }

    private val gson = Gson()

    private val sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    private fun getString(key: String): String = sharedPreferences.getString(key, EMPTY_STRING)

    private fun setString(key: String, value: String) {
        sharedPreferences.edit()
                .putString(key, value)
                .apply()
    }

    private fun removeKey(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }


    override fun setTeacher(teacher: Teacher) = setString(TEACHER_KEY, gson.toJson(teacher))
    override fun getTeacher(): Teacher = gson.fromJson(getString(TEACHER_KEY), Teacher::class.java)
    override fun hasTeacher(): Boolean = sharedPreferences.contains(TEACHER_KEY)
    override fun removeTeacher() = removeKey(TEACHER_KEY)

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }
}