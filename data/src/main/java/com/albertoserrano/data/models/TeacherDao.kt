package com.albertoserrano.data.models

import com.albertoserrano.domain.constants.Constants.Companion.DEFAULT_LONG
import com.albertoserrano.domain.constants.Constants.Companion.EMPTY_STRING
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class TeacherDao(
        @PrimaryKey var id: Long = DEFAULT_LONG,
        var role: String = EMPTY_STRING,
        var name: String = EMPTY_STRING,
        var lastName: String = EMPTY_STRING,
        var email: String = EMPTY_STRING,
        var school: Long? = DEFAULT_LONG
) : RealmObject()