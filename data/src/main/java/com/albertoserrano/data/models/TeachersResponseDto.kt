package com.albertoserrano.data.models


data class TeachersResponseDto(
        val teachers: List<TeacherDto>
)

data class TeacherDto(
        val id: Long,
        val role: String,
        val name: String,
        val lastName: String,
        val email: String,
        val school: Long
)