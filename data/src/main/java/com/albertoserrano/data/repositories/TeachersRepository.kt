package com.albertoserrano.data.repositories

import com.albertoserrano.data.datasources.local.DatabaseRepository
import com.albertoserrano.data.datasources.remote.NetworkRepository
import com.albertoserrano.data.datasources.settings.Settings
import com.albertoserrano.domain.models.Teacher
import com.albertoserrano.domain.repository.TeacherRepository
import io.reactivex.Completable
import io.reactivex.Single

class TeachersRepository(
        private val remote: NetworkRepository,
        private val local: DatabaseRepository,
        private val settings: Settings) : TeacherRepository {

    override fun retrieveTeacher(id: Long): Single<Teacher> =
            local.getTeacher(id).flatMapSingle {
                Single.just(it)
            }

    override fun login(id: Int): Completable =
            remote.doLogin(id)
                    .flatMapCompletable { teacher ->
                        settings.setTeacher(teacher)
                        local.getTeacher(teacher.id).isEmpty.flatMapCompletable { local.saveTeachers(mutableListOf(teacher)) }
                        return@flatMapCompletable Completable.complete()
                    }

    override fun retrieveTeachers(): Single<List<Teacher>> =
            local.getTeachers().flatMapSingle {
                if (it.isNotEmpty()) {
                    Single.just(it)
                } else {
                    remote.retrieveTeachers()
                            .map { teachers ->
                                local.saveTeachers(teachers)
                                //    settings.setTeacher(teachers.first())
                                return@map teachers
                            }
                }
            }

    override fun currentTeacher(): Teacher =
            settings.getTeacher()


    override fun modifyTeacher(teacher: Teacher): Completable {
        if (settings.getTeacher().id == teacher.id) {
            settings.removeTeacher()
            settings.setTeacher(teacher)
        }
        return local.saveTeacher(teacher)
    }
}