package com.albertoserrano.data

import android.util.Log
import io.realm.Realm
import io.realm.RealmObject

/**
 * AndroidExtensions
 */

/**
 * Any
 * */
fun Any.info(text: String) {
    Log.i(this::class.java.simpleName, text)
}

fun Any.error(text: String) {
    Log.e(this::class.java.simpleName, text)
}

fun Any.error(text: String, exception: Exception) {
    Log.e(this::class.java.simpleName, text, exception)
}

fun Any.warn(text: String) {
    Log.w(this::class.java.simpleName, text)
}

fun Any.warn(text: String, exception: Exception) {
    Log.w(this::class.java.simpleName, text, exception)
}

fun Any.debug(text: String) {
    Log.d(this::class.java.simpleName, text)
}

fun Any.debug(text: String, exception: Exception) {
    Log.d(this::class.java.simpleName, text, exception)
}


fun <T : RealmObject> List<T>.save() {
    val realm = Realm.getDefaultInstance()
    realm.use {
        it.executeTransaction {
            it.copyToRealmOrUpdate(this)
        }
    }
}

fun <T : RealmObject> T.save() {
    val realm = Realm.getDefaultInstance()
    realm.use {
        it.executeTransaction {
            it.copyToRealmOrUpdate(this)
        }
    }
}

fun <T : RealmObject> T.nextId(): Long {
    val realm = Realm.getDefaultInstance()
    val max = realm.where(this::class.java).max("id")
    return max?.toLong()?.plus(1) ?: 1
}

fun <T : RealmObject> removeAll(realmObject: Class<T>) {
    val realm = Realm.getDefaultInstance()
    realm.use {
        it.executeTransaction {
            it.where(realmObject).findAll().deleteAllFromRealm()
        }
    }
}