package com.albertoserrano.data.mappers.dto

import com.albertoserrano.data.models.TeacherDto
import com.albertoserrano.domain.models.Teacher


fun TeacherDto.toTeacher(): Teacher = Teacher(
        id = id,
        name = name,
        lastName = lastName,
        email = email
)