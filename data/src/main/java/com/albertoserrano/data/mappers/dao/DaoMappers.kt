package com.albertoserrano.data.mappers.dao

import com.albertoserrano.data.models.TeacherDao
import com.albertoserrano.domain.constants.Constants
import com.albertoserrano.domain.models.Teacher

fun Teacher.toTeacherDao(): TeacherDao = TeacherDao(
        id = id,
        role = role,
        name = name ?: Constants.EMPTY_STRING,
        lastName = lastName ?: Constants.EMPTY_STRING,
        email = email ?: Constants.EMPTY_STRING,
        school = school
)

fun TeacherDao.toTeacher(): Teacher = Teacher(
        id = id,
        name = name,
        lastName = lastName,
        role = role,
        email = email
)