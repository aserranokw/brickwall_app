package com.albertoserrano.domain.repository

import com.albertoserrano.domain.models.Teacher
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Repository.
 */
interface TeacherRepository {

    fun currentTeacher(): Teacher
    fun retrieveTeachers(): Single<List<Teacher>>
    fun login(id: Int): Completable
    fun modifyTeacher(teacher: Teacher): Completable
    fun retrieveTeacher(id: Long): Single<Teacher>
}