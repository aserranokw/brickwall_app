package com.albertoserrano.domain.models

import com.albertoserrano.domain.constants.Constants

data class Teacher(
        var id: Long = Constants.DEFAULT_LONG,
        var role: String = Constants.EMPTY_STRING,
        var name: String? = null,
        var lastName: String? = null,
        var email: String? = null,
        // TODO  var school: School? = null
        var school: Long? = null
)

object Role {
    val ROLE_EDUCATOR = "educator"
    val ROLE_MANAGER = "manager"
    val ROLE_ADMIN = "admin"
}
