package com.albertoserrano.domain.constants

/**
 * Constants
 */
class Constants {
    companion object {
        val EMPTY_STRING: String = ""

        val DEFAULT_LONG: Long = 0

        val DEFAULT_INT: Int = 0

        fun preferencesName(buildType: BuildType): String = when (buildType) {
            BuildType.MOCK -> "bw_mock"
            BuildType.DEBUG -> "bw_debug"
            BuildType.RELEASE -> "bw"
        }

        fun notificationChannelId(buildType: BuildType): String = when (buildType) {
            BuildType.MOCK -> "bw_mock"
            BuildType.DEBUG -> "bw_debug"
            BuildType.RELEASE -> "bw"
        }
    }
}

fun buildType(type: String): BuildType = when (type) {
    "mock" -> BuildType.MOCK
    "debug" -> BuildType.DEBUG
    else -> BuildType.RELEASE
}

enum class BuildType {
    MOCK, DEBUG, RELEASE
}
