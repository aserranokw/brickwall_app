package com.albertoserrano.domain.interactor.usecases

import com.albertoserrano.domain.exceptions.PermissionDenied
import com.albertoserrano.domain.executor.Executor
import com.albertoserrano.domain.interactor.CompletableInteractor
import com.albertoserrano.domain.models.Role
import com.albertoserrano.domain.models.Teacher
import com.albertoserrano.domain.repository.TeacherRepository
import io.reactivex.Completable

class ModifyTeacherUseCase(private val repository: TeacherRepository, executor: Executor) : CompletableInteractor(executor = executor) {

    fun execute(teacher: Teacher, onComplete: () -> Unit, onError: (Throwable) -> Unit) {
        super.execute(
                onComplete = onComplete,
                onError = onError,
                completable = modify(teacher)
        )
    }

    private fun modify(teacher: Teacher): Completable =
            if (isModifiable(teacher))
                repository.modifyTeacher(teacher)
            else
                Completable.error(PermissionDenied())


    fun isModifiable(targetTeacher: Teacher): Boolean {
        val currentTeacher: Teacher = repository.currentTeacher()
        return if (isItself(targetTeacher, currentTeacher))
            true
        else
            when (currentTeacher.role) {
                Role.ROLE_ADMIN -> true
                Role.ROLE_MANAGER ->
                    if (targetTeacher.role == Role.ROLE_EDUCATOR)
                        currentTeacher.school == targetTeacher.school
                    else false
                else -> false
            }
    }

    private fun isItself(teacherTarget: Teacher, currentTeacher: Teacher): Boolean =
            teacherTarget.id == currentTeacher.id

    fun isRemovable(teacher: Teacher): Boolean =
            if (teacher.role == Role.ROLE_EDUCATOR)
                isModifiable(teacher)
            else
                false

}
