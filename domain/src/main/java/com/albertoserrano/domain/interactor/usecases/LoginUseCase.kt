package com.albertoserrano.domain.interactor.usecases

import com.albertoserrano.domain.executor.Executor
import com.albertoserrano.domain.interactor.CompletableInteractor
import com.albertoserrano.domain.repository.TeacherRepository

class LoginUseCase(private val repository: TeacherRepository, executor: Executor) : CompletableInteractor(executor = executor) {

    fun execute(id: Int, onComplete: () -> Unit, onError: (Throwable) -> Unit) {
        super.execute(
                onComplete = onComplete,
                onError = onError,
                completable = repository.login(id))
    }
}
