package com.albertoserrano.domain.interactor.usecases

import com.albertoserrano.domain.executor.Executor
import com.albertoserrano.domain.interactor.SingleInteractor
import com.albertoserrano.domain.models.Teacher
import com.albertoserrano.domain.repository.TeacherRepository

class RetrieveTeacherUseCase(private val repository: TeacherRepository, executor: Executor) :
        SingleInteractor<Teacher>(executor = executor) {

    fun execute(id: Long, onSuccess: (Teacher) -> Unit, onError: (Throwable) -> Unit) {
        super.execute(
                onSuccess = onSuccess,
                onError = onError,
                single = repository.retrieveTeacher(id))
    }

}