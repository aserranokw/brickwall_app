package com.albertoserrano.domain.interactor.usecases

import com.albertoserrano.domain.executor.Executor
import com.albertoserrano.domain.interactor.SingleInteractor
import com.albertoserrano.domain.models.Teacher
import com.albertoserrano.domain.repository.TeacherRepository

class RetrieveTeachersUseCase(private val repository: TeacherRepository, executor: Executor) :
        SingleInteractor<List<Teacher>>(executor = executor) {

    fun execute(onSuccess: (List<Teacher>) -> Unit, onError: (Throwable) -> Unit) {
        super.execute(
                onSuccess = onSuccess,
                onError = onError,
                single = repository.retrieveTeachers())
    }

}