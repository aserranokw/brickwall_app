package com.albertoserrano.domain.exceptions

class PermissionDenied : Exception()